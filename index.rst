.. sphinx-pupbef documentation master file, created by
   sphinx-quickstart on Wed Apr 22 06:01:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. sidebar:: ACHTUNG

  Dies ist eine Arbeitsversion.


Puppet-Befehlsliste
===================

:Release: |version|
:Date: |today|

Das Dokument wird unter der `MIT <https://opensource.org/licenses/MIT>`_ Lizenz zur Verfügung gestellt.

.. toctree::
  :maxdepth: 2

  text/01-06/01-dokument
  text/01-06/02-aufbau
  text/01-06/03-einstellung
  text/01-06/04-@befehle
  text/01-06/05-sprachelemente
  text/01-06/06-actions

  text/07/07-befehle
  text/08/08-ausdruecke

  text/09-12/09-variablen
  text/09-12/10-debugging
  text/09-12/11-puppets
  text/09-12/12-ausfuehrung


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
