
*******
Puppets
*******

Hier einige Anmerkungen zu Puppets im Allgemeinen.




.. index:: pair: Puppets; Puppet-Zeichensatz

Puppet-Zeichensatz
==================

(Zitat SLC:) There are now several possibilities to use multibyte character source code:

* A line in the sourcecode (best directly after "``PUPPET xyz``") like:

  - ``CHARSET: "ISO-8859-1"``
  - ``CHARSET: "UTF-8"``
  - ``CHARSET: "Shift_JIS"``

* A modification of the registration:

  - ``/puppetdef set PUPPETNAME charset="ISO-8859-1"``
  - ``/puppetdef set PUPPETNAME charset="UTF-8"``
  - ``/puppetdef set PUPPETNAME charset="Shift_JIS"``

* A given URL that ends with "\_UTF-8" (UTF-8 only)

  - ``/puppetdef set PUPPETNAME url="http://my.computer/puppet.prog_UTF-8"``
  - ``/puppet start PUPPETNAME``
  - ``/puppet CommandPuppet PUPPETNAME http://my.computer/puppet.prog_UTF-8``

* Certain UTF-8 markers (UTF-8 only, unsafe)

  - ``/puppetdef set PUPPETNAME url="http://my.computer/puppet.prog_UTF-8"``
  - ``/puppet start PUPPETNAME``
  - ``/puppet CommandPuppet PUPPETNAME http://my.computer/puppet.prog_UTF-8``


*Examples*:

- ``/puppet TP CantStop-ja``
- ``/puppet TP ParisParis-zh-tr``

:Quelle: BSW Puppet Forum >> How to use a codepage/charset other than ISO-8859-1 (latin1) => UTF-8, Shift_JIS




.. index:: pair: Puppets; Puppet-Registrierung

Puppet-Registrierung
====================

Mit dem Befehl ``/puppetdef`` können Daten zu einem Puppet unter einem Namen auf dem Server hinterlegt werden. Normalerweise gibt man die Parameter einfach so wie bei ``/puppet`` an.


Puppet Registrierung anlegen
----------------------------

::

  /puppetdef CommandPuppet <NAME> <URL>

* Puppet wird unter dem angegebenen Namen registriert, wobei es beim Start von der ebenfalls angegebenen URL geladen wird.
* Die URL kann sich auch PHP-Skripte referenzieren. Dann wird die URL in der Form ``http://server/seite.php?parameter=wert`` angegeben.

:Quelle: BSW Puppet Forum >> URL registrierter Puppets wird bei Gleichheitszeichen abgeschnitten



Puppet Registrierung anzeigen
-----------------------------

::

  /puppetdef info <NAME>



Puppet Registrierung löschen
----------------------------

::

  /puppetdef delete <NAME>



Puppet Registrierung ändern
---------------------------

Beim Unterbefehl ``set`` können mehrere Werte gleichzeitig gesetzt werden, immer mit ``<NAME>=<WERT>`` (ohne Leerzeichen). Enthält der Wert Leerzeichen, so müssen Anführungszeichen benutzt werden: ``<NAME>="<WERT>"``.


*Typ des Puppets setzen*:

::

  /puppetdef set <NAME> type=<TYPE>


*WWW-Adresse für den Quellcode des Puppets setzen*:

Der Quellcode (das Programm in BSW-Puppetsprache) muss als pure ASCII-Datei auf einem Web-Server hinterlegt werden.

::

  /puppetdef set <NAME> url=<URL>


*Puppet als "privat" deklarieren*:

Starten des Puppets ist auf die angegebenen Starter begrenzt.

::

  /puppetdef set <NAME> priv (oder "public=Off" )


*Puppet als "public" deklarieren*:

Auch andere Benutzer als die bei "Starter:" angegebenen können dieses Puppet starten, ohne z.B. die URL zu kennen. In diesem Fall werden URL, Kennwort (PWD), Levelpunkte und Stack nicht angezeigt die Karteikarten "debug" und "Statistik" bleiben inaktiv.

::

  /puppetdef set <NAME> public (oder "public=On")


*Puppet als "public" deklarieren, aber ein Passwort festlegen*:

Jeder kann das Puppet starten, der das zugehörige Passwort kennt.

::

  /puppetdef set <NAME> public pwd=<PWD>


*Kurzinformation für die Puppet-Registrierung setzen*:

::

  /puppetdef set <NAME> info="INFO"




.. index:: pair: Puppets; Puppet-Typen

Puppet-Typen
============

SimplePuppets
-------------

SimplePuppets kann man i.W. ferngesteuert durch die BSW schicken::

  /puppet SimplePuppet Marionette
  /.Marionette /room C88-19
  /.Marionette Huhu, ich bin von SLC geschickt worden
  /unown Marionette
  /own Marionette
  /.Marionette /sound off
  /.Marionette /twindow on
  /puppet stop Marionette


Einige Möglichkeiten bietet auch das PuppetTool, das man sich über rechter Mausklick -> "Configure..." in die Toolleiste holen kann:

* SimplePuppet kann im Befehl ``/puppet`` mit SP abgekürzt werden.
* SimplePuppets empfangen keine YELLs, da der Owner die YELLs in gewünschter Stufe selbst hören - und das Puppet die Informationen nicht verarbeiten kann.

:Quelle: BSW Puppet Forum >> Simple puppets - Antwort #2


CommandPuppets
--------------

* CommandPuppet kann im Befehl ``/puppet`` mit CP abgekürzt werden.
* Puppet-Version mit altem (bekannt lauffähigem Code), auf die zurückgefallen werden kann, wenn mal etwas nicht klappt. Soll Dauerausfälle verhindern...


AutoStartPuppets
----------------

* AutoStartPuppets müssen durch SLC als solche freigeschaltet werden (SLC@brettspielwelt.de).
* Nach vollständiger Registrierung (mit URL) und erfolgter Freischaltung einmal mit ``/puppet start <name>`` starten, um das Anstarten nach einem Server-Reboot zu aktivieren.
* AutoStartPuppets existieren zwar ab Serverneustart (auch im PuppetTool) ... werden aber erst rund 5 Minuten später aktiv (geladen).
* Der Start dieser Puppets ist auf 5 Minuten gestreckt (damit über 3 Sekunden pro Puppetstart im Mittel).
* AutoStartPuppets können grundsätzlich nur vom Starter, den übrigen Startern, Admins oder dem Server gekickt werden; kickt jemand anders das Puppet, so begibt es sich in den eigenen Startraum (Ausnahme: Levelpunkte runtersetzen lassen).
* AutoStartPuppetskönnen grundsätzlich nicht gebeamt/gesummont werden (Ausnahme: Levelpunkte runtersetzen lassen).
* AutoStartPuppets können shouten.
* Will man den Autostart-Mechanismus ausstellen, so muß man einmalig das Puppet ohne URL aufrufen, d.h. in der Regel ``/puppet AutoStartPuppet <name>``.


  .. tip::

    Für die Berechtigung zum Autostart schickt man eine Antragsmail an "SLC@brettspielwelt.de". Die Antragsmail soll folgenden Inhalt haben: Username des Starters, Name des Puppets, URL und Begründung, weshalb dies ein AutoStartPuppet werden soll. - Hierzu gibt es auch einen gepinnten Thread im "Puppet" Forum.

    *Hinweis*: In 2020 ist diese Aussage vermutlich veraltet.


Puppet "Puppetier"
------------------

Alternativ kann man ein Puppet aber auch durch ein anderes ASP mitstarten lassen. Dies ist z.B. über das Puppet "Puppetier" möglich.


Erklärpuppets
-------------

Erklärpuppets werden grundsätzlich so aufgerufen::

  /puppet TutorPuppet [<spielname>]
  /puppet TP [<spielname>]

Dabei ist <spielname> der Name der Gilde des Spiels, muss aber nicht angegeben werden, da ohne Parameter einfach das Spiel im Raum genommen wird.

  .. note::

    Das Puppet kann den Raum nicht wechseln. Wenn alle Spieler den Raum verlassen haben, verschwindet das Puppet.


Turnierpuppets (von BloodyMary)
-------------------------------

loodyMarys Turnier-Puppet sind auf dem BSW Server hinterlegt. Um diese zu nutzen wird eine symbolische URL genutzt.

::

  /puppet CommandPuppet MeineLiga <turnier>
  /puppetdef CommandPuppet MeineLiga <turnier>
  /puppetdef set MeineLiga url=<turnier>


Als Angabe &lt;turnier&gt; können folgende Optionen angegeben werden:

- ``turnier/3`` - Version 3 der Puppets
- ``turnier/4`` - Version 4 der Puppets
- ``turnier/4beta`` - Version 4 (beta) der Puppets

:Quelle: BSW Puppet Forum >> Turnierpuppets starten nicht - Antwort #6




.. index:: pair: Puppets; Puppet-Anzeigen

Puppet-Anzeigen
===============

Anzeige Load- und Mem-% in "/who"
---------------------------------

Bei ``/who`` u.ä. sieht der STARTER des Puppets, wie beschäftigt das Puppet ist und wieviel Speicher es verbraucht.


Anzeigen im Puppettool
----------------------

MaxCommand
  Dies ist in der Regel ein Wert zwischen 0 und 100 und gibt die Anzahl der in einem (Takt-)Zyklus maximal ausgeführten wesentlichen Operationen wieder. Im Singlestep-Modus (siehe Debugging) wird trotzdem nur 1 Schritt ausgeführt.\

  *Startwert*: 10

Abarbeit
  Es stehen je nach Belastung des Servers 1/n (n = NICE-Wert, n = 20) der Dauer eines Zyklus (TICKER, normalerweise 1 sek.) zur Verfügung. Das wären bei diesen Werten also 50 ms. Bleibt die Ausführungszeit unterhalb dieser Grenze, schläft das Puppet 950 ms und macht danach mit voller Power weiter. Bei Überschreitung wird die überschüssige Zeit auf Abarbeit draufgeschlagen, das Puppet zur Pause verdammt und der Wert in 50 (ms) Schritten wieder abgebaut. Außerdem wird MaxCommand negativ beeinflußt (z.B. 100 -> 99). Dadurch werden zwar die Überschreitungen möglicherweise reduziert, es führt jedoch bei dauerhafter Überschreitung zu Hemmnissen bis zum Stop des Puppets.\

  *Startwert*: -

%load
  Bei MaxCommand zwischen 0 und 100 ist %load = 100 - MaxCommand. Startwert ist 90%, es dauert also eine Weile, bis ein Puppet mit voller Power läuft. Zurecht, denn das Laden des Codes und das Parsen belastet den Server nicht unerheblich.\

  *Startwert*: 90%

%mem
  Prozentualer Anteil des genutzten Speicher am Gesamtspeicher. Als Speicher gelten hier alle möglichen Größen wie z.B. Variablen, Ausdrücke, Eventhandler, Actions, usw.\

  *Startwert*: -


Darüber hinaus werden noch die folgenden (hier nicht weiter erläuterten) Werte angezeigt:

* Codeblöcke
* Variablen
* Ausdrücke
* Stack
* Speicher
* SleepTo
* NextScope
* GlobalLine
* ACTION
* Kicker

:Quelle: BSW Puppet Forum >> Parameter des Puppet-Tools ? - Antwort #1
