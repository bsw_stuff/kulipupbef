
.. _debugging:

*********
Debugging
*********

Enthält das Puppet die Einstellung ``DEBUG``, so wird mit dem Start des Puppets ein Debugging-Fenster geöffnet. In diesem Fenster wird unterschiedliche Debugging-Information ausgegeben.

Welche Informationen dies sind, steuert die Liste, die man bei der DEBUG-Einstellung angibt. Diese kann zwischen keinem und allen der folgenden Werte enthalten:

* ``ACTION``: Meldet, wenn Aufrufe von Actions ausgelöst werden.
* ``EVENT``: Meldet Ereignisse wie Chatmeldungen, auftauchende Spieler, Ablauf des Timer-Events, usw., die ggfs.  Actions auslösen.
* ``VARIABLES``: Zeigt Änderungen von Variablen an, allerdings nur für einen im Programm mit ``DEBUG ON/OFF`` umschlossenen Bereich (wegen der auftretenden  Datenmenge).
* ``SINGLESTEP``: Einzelschrittmodus mit Angabe jedes einzelnen Befehls, allerdings wieder nur in bestimmten Bereichen des Programms.
* ``LOCAL``: Zeigt Ort und Werte lokaler Variablen (``LOCAL``-Befehl) am Anfang  und Ende der Lebensdauer an.
* ``SCOPE``: Jeder Unteraufruf von Actions oder Begin/End-Blöcken wird automatisch durchnummeriert. Hier sieht  man, wie das Programm in solche Blöcke rein- und rausspringt.
* ``TOOL``: ...

Die Optionen ``SINGLESTEP`` und ``LOCAL`` werden nur aktiv, wenn im Programm irgendwo der Befehl ``DEBUG ON`` steht. Mit ``DEBUG OFF`` kann man die Wirkung dieser beiden Werte wieder deaktivieren und damit auf Bereiche des Puppets eingrenzen.

``DEBUG: ""`` ist nicht identisch mit ``DEBUG: "ON"`` (bzw. ``DEBUG: ON``). Letzteres aktiviert zusätzlich das Flag für die empfindlichen / ausufernden Ausgaben wie z.B. bei ``SINGLESTEP``.




.. index:: pair: Debugging-Modus; Debug

.. _debuggingmodus:

Debugging-Modus
===============

Der **Starter eines Puppets** kann die Angabe von ``DEBUG:<modes>`` unter den :ref:`Puppeteinstellungen <puppeteinstellungen>` dynamisch ändern:

* ``@@debug ON``: Debugging-Modus anschalten.
* ``@@debug OFF``: Debugging-Modus ausschalten.
* ``@@debug ACTION``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Actions  ausgegeben.
* ``@@debug EVENT``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Events ausgegeben.
* ``@@debug VARIABLES``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Variablen ausgegeben.
* ``@@debug LOCAL``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von lokalen Variablen ausgegeben.
* ``@@debug SINGLESTEP``: Bei angeschaltetem Debugging-Modus werden Informationen auf der Ebene von Einzelschritten ausgegeben.
* ``@@debug SCOPE``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung von Scopes ausgegeben.
* ``@@debug TOOL``: Bei angeschaltetem Debugging-Modus werden Informationen zur Handhabung der Tools ausgegeben.




Ausgaben der DEBUG-Einstellungen
================================


.. index:: pair: Debug ACTION; Debug

Debug ACTION
------------

Der Aufruf einer Action und die Ausführung der ``CASE``-Zweige eines ``SWITCH`` werden angezeigt.

::

  A <action>
  A <action> (sofort)
  SWC ...
  SWA ...

* Kennung =

  * ``A`` = Action
  * ``SWC`` = ``CASE``
  * ``SWA`` = ``CASEALL``

* <action> = Name der Action
* (sofort) = Action wird vorrangig ausgeführt (siehe etwa ``WHEN KICKED``, aber auch ``DO``)



.. index:: pair: Debug EVENT; Debug

Debug EVENT
-----------

Jedes Mal, wenn ein Event auftritt (unabhängig davon, ob der Event auch mit einem ``WHEN``-Befehl abgefragt wird) wird ein

::

  E: <typ> <weitere Infos>

ausgegeben. <typ> gibt an, was für ein Event es ist, und abhängig davon, gibt <weitere Infos> weitere Informationen dazu an.

|more| Vergleiche hierzu die Beschreibung des :ref:`EVENTDUMP-Befehl <befehlEVENTDUMP>`.



.. index:: pair: Debug VARIABLES; Debug

Debug VARIABLES
---------------

Gibt bei jeder Änderung einer Variablen aus::

  V <name>: <alterwert> => <neuerwert>



.. index:: pair: Debug LOCAL; Debug

Debug LOCAL
-----------

Liefert Informationen über Anfang und Ende lokaler Variablen. Ausgabe zum Beispiel so:

  L + Label 337 in Zeile 564 angelegt: (LOCALLABEL) <= Label 0: (LOCAL 2. mit Parameter)
  L - Label0 337 in Zeile 567 entfernt: (LOCALLABEL)

* Zunächst wird die lokale Variable "Label" in Zeile 564 angelegt (ID=337) und mit "LOCALLABEL" initialisiert, wodurch die globale Variable (immer ID=0) mit dem Wert "LOCAL 2. mit Parameter" erst einmal verborgen wird.
* Später, in Zeile 567, wird dann der alte Wert hervorgekramt...


.. note::

  Es sind auch Rekursionen möglich, die in jeder Stufe eine neue lokale Variable erzeugen.


.. tip::

  ``@@debug LOCAL`` kann genutzt werden, um die von den Befehlen gesetzten Variablen zu ermitteln (wenn die Befehlsliste unvollständig ist).



.. index:: pair: Debug SINGLESTEP; Debug

Debug SINGLESTEP
----------------

.. warning::

  ``SINGLESTEP`` kann sehr viel Ausgabe erzeugen und die Ausführung des Programms drastisch verlangsamen. Es ist deshalb ratsam, nur den interessanten Teil des Codes durch das Kommando ``DEBUG ON/OFF`` debuggen zu lassen.



Ausgabe für Befehle
-------------------

Gibt jeden einzelnen Befehl aus::

  C <name>: <parameter> (<Zeilennummer>)

Bei den Parametern kann es sein, dass ein `#<zahl>` angegeben wird. Dabei handelt es sich um den (internen) Namen von BEGIN/END-Blöcken, die intern wie eigene Actions behandelt werden. Weiterhin wird bei Ausdrücken nur eine Zahl angegeben. Diese ist ebenfalls ein (interner) Name für den zugehörigen Ausdruck.

Gelegentlich werden weitere Befehle ausgegeben, die nur intern sind, wie etwa ``ERROR ON/OFF``, die dazu da sind, dass innerhalb einer Error-Action diese nicht wieder aufgerufen werden kann, etc.



Ausgabe für Ausdrücke
---------------------

Weiterhin gibt dieser Befehl auch die Auswertung von Ausdrücken an. Dies geschieht durch::

  EX <name>: <parameter>


*Beispiel*:

Hierbei wird der Ausdruck in Einzelteile zerlegt. Die Auswertung von::

  EVAL x = ( 3 + 5 ) * ( FIRSTOF WITHOUTFIRST "genau 100 Gramm Mehl" )

führt zu folgender Debug-Ausgabe::

  C EVAL: >x< >6< (7)
  EX +: >3< >5<
  EX WITHOUTFIRST: >genau 100 Gramm Mehl<
  EX FIRSTOF: >100 Gramm Mehl<
  EX *: >8< >100<

Dies bedeutet: Der ``EVAL``-Befehl hat zwei Parameter, nämlich "x" (die Variable in der das Ergebnis gespeichert werden soll) und "6" (den internen Namen des Ausdrucks, der gleich ausgewertet werden wird).

Dieser Ausdruck berechnet das Produkt von zwei Zahlen (genauer wieder Ausdrücken), nämlich ( 3 + 5 ) und ( ``FIRSTOF WITHOUTFIRST`` "genau 100 Gramm Mehl" ). Bevor dieses Produkt ausgewertet werden kann, müssen die beiden Faktoren berechnet werden. Die ``EX``-Zeilen geben genau die Reihenfolge an, in der die Auswertung stattfindet.



.. index:: pair: Debug Beispiele; Debug

Debug - Beispiele
-----------------

Der Aufruf einer Action und die Ausführung der ``CASE``-Zweige eines ``SWITCH`` werden dargestellt.

Aufgebreitetes Beispiel::

  (13:08:08) A where
  (13:08:08) C SET ATUSER: >SLC< (111)
  (13:08:09) C SETLOCAL: >PARAM< >< (111)
  (13:08:10) V PARAM: {} => {}
  (13:08:10) C SETLOCAL: >WHO< >SLC< (111)
  (13:08:11) V WHO: {SLC} => {SLC}
  (13:08:11) C ACTIONBEGIN: >where< (47)
  (13:08:12) S > Scope 11 von Scope 0 in Zeile 47 gestartet
  (13:08:13) C LOCAL: >raum< >0< (48)
  (13:08:13) L + raum 11 in Zeile 48 angelegt: ()
  (13:08:13) V raum: => {}
  (13:08:14) C GETROOMINFO: (49)
  (13:08:14) L + ROOM 11 in Zeile 49 angelegt: (43)
  (13:08:14) V ROOM: => {43}
  (13:08:14) L + NAME 11 in Zeile 49 angelegt: (Bei dem Manager)
  (13:08:14) V NAME: => {Bei dem Manager}
  (13:08:14) L + GAME 11 in Zeile 49 angelegt: (Manager2)
  (13:08:14) V GAME: => {Manager2}
  (13:08:14) L + OWNER 11 in Zeile 49 angelegt
  (13:08:14) V OWNER: =>
  (13:08:17) C WHEREIS: >PUPPET< (50)
  (13:08:17) V ROOM: {43} => {43}
  (13:08:18) C IF: >3< >#0< (51)
  (13:08:18) EX ISEMPTY: ><
  (13:08:18) EX NOT: >TRUE<
  (13:08:19) C LOCAL: >antwort< >4< (53)
  (13:08:19) L + antwort 11 in Zeile 53 angelegt: (42)
  (13:08:19) V antwort: => {42}
  (13:08:20) C SWITCH: >5< >#1< (54)
  (13:08:20) C CASE: >C*-*-*< >#2< (55)
  (13:08:20) C CASE: >C*-*< >#3< (58)
  (13:08:20) C CASE: >C*< >#4< (61)
  (13:08:20) C CASE: >0< >boerse< (64)
  (13:08:20) C CASE: >16< >#5< (66)
  (13:08:20) C CASE: >28< >manager< (68)
  (13:08:20) EX +: >42< >1<
  (13:08:20) C CASE: >43< >manager< (69)
  (13:08:20) C CASEALL: >*< >#7< (75)
  (13:08:20) SWC >manager< 43 >43< [0] (69)
  (13:08:20) SWA >#7< 43 >*< [1] >43< (75)
  (13:08:20) A manager (sofort)
  (13:08:22) C ACTIONBEGIN: >#1< (54)
  (13:08:22) S > Scope 12 von Scope 11 in Zeile 54 gestartet
  (13:08:23) C SETLOCAL: >SUBSTLEN< >0< (69)
  (13:08:23) L + SUBSTLEN 12 in Zeile 69 angelegt: (0)
  (13:08:23) V SUBSTLEN: => {0}
  (13:08:24) C ACTIONBEGIN: >manager< (85)
  (13:08:24) S > Scope 13 von Scope 12 in Zeile 85 gestartet
  (13:08:25) C >>: >/tell SLC 43 ist der Spielemanager in ARMfeld< (86)
  (13:08:26) E TELL: Du sagst: 43 ist der Spielemanager in ARMfeld, SLC, 43, Bei dem Manager
  (13:08:26) C ACTIONEND: >manager< (87)
  (13:08:26) S < Rücksprung von Scope 13 nach Scope 12 in Zeile 87
  (13:08:27) C >>: >/tell SLC 0 Teile - "" "" "" - Spiel(e): Manager2 - Name: Bei dem Manager - Owner: < (74)
  (13:08:28) E TELL: Du sagst: 0 Teile - "" "" "" - Spiel(e): Manager2 - Name: Bei dem Manager - Owner:, SLC, 43, Bei dem Manager
  (13:08:28) C SETLOCAL: >SUBST1< >43< (75)
  (13:08:28) L + SUBST1 12 in Zeile 75 angelegt: (43)
  (13:08:28) V SUBST1: => {43}
  (13:08:29) C SETLOCAL: >SUBSTLEN< >1< (75)
  (13:08:29) V SUBSTLEN: {0} => {1}
  (13:08:30) C ACTIONBEGIN: >#7< (76)
  (13:08:30) S > Scope 14 von Scope 12 in Zeile 76 gestartet
  (13:08:31) C >>: >/tell SLC <EOT>< (76)
  (13:08:32) E TELL: Du sagst: <EOT>, SLC, 43, Bei dem Manager
  (13:08:33) C ACTIONEND: >#7< (77)
  (13:08:33) S < Rücksprung von Scope 14 nach Scope 12 in Zeile 77
  (13:08:34) C ACTIONEND: >#1< (78)
  (13:08:34) L - SUBSTLEN 12 in Zeile 78 entfernt: (1)
  (13:08:34) V SUBSTLEN: {1} =>
  (13:08:34) L - SUBST1 12 in Zeile 78 entfernt: (43)
  (13:08:34) V SUBST1: {43} =>
  (13:08:34) S < Rücksprung von Scope 12 nach Scope 11 in Zeile 78 (SUBST1)
  (13:08:36) C ACTIONEND: >where< (79)
  (13:08:36) L - raum 11 in Zeile 79 entfernt: ()
  (13:08:36) V raum: {} =>
  (13:08:36) L - ROOM 11 in Zeile 79 entfernt: (43)
  (13:08:36) V ROOM: {43} =>
  (13:08:36) L - NAME 11 in Zeile 79 entfernt: (Bei dem Manager)
  (13:08:36) V NAME: {Bei dem Manager} =>
  (13:08:36) L - GAME 11 in Zeile 79 entfernt: (Manager2)
  (13:08:36) V GAME: {Manager2} =>
  (13:08:36) L - OWNER 11 in Zeile 79 entfernt
  (13:08:36) V OWNER: =>
  (13:08:36) L - antwort 11 in Zeile 79 entfernt: (42)
  (13:08:36) V antwort: {42} =>
  (13:08:36) S < Rücksprung von Scope 11 nach Scope 0 in Zeile 79 (antwort)
  (13:08:38) C CLEAR ATUSER: (111)


Dazu passender Puppetcode::

  1 PUPPET TestSwitch
  2
  3 # Puppet TestSwitch.prog
  4 #
  5 # (c) 2006 SLC@BSW (S.Loges@gmx.de)
  6 #
  7 # Puppet zum Testen von SWITCH und CASE
  8 #
  9 # 060807 Startversion
  10 #
  11 #######################################
  12
  13 LOGIN: " startet"
  14 LOGOUT: " verschwindet"
  15 APPEAR: " kommt"
  16 DISAPPEAR: " geht"
  17 INFO: "Puppet zum Testen von SWITCH und CASE."
  18 OVERFLOW: ERROR
  19 OWN: NO
  20 NEWOWNER: NO
  21 CITYCHAT: NO
  22 CASESENSITIV: YES
  23 #SAVE: "*"
  24 DEBUG: "ON"
  25
  26 @where: where
  27 @vdump: varDump
  28 @edump: eventDump
  29 @qdump: queueDump
  30
  31 ACTION start
  32   WHEN ERROR DO error
  33   DO initPuppet
  34   >> :ist geladen und startbereit!
  35 END
  36
  37 ACTION initPuppet
  38   WHOIS STARTER
  39   SET Meister [WHO]
  40   SET SELF TestSwitch
  41
  42   >> /name [SELF]
  43
  44   WHEN KEYWORD "@where" DO where
  45 END
  46
  47 ACTION where
  48   LOCAL raum = [ROOM]
  49   GETROOMINFO
  50   WHEREIS PUPPET
  51   IF NOT ISEMPTY [raum]
  52     SET ROOM [raum]
  53   LOCAL antwort = 42  # [ROOM] und auch [antwort] müssen bereits beim SWITCH belegt sein!
  54   SWITCH [ROOM]
  55     CASE "C*-*-*"
  56       >> /tell [Meister] [ROOM] ist in einer Burg/Kathedrale oder im Olympiastadion
  57     END
  58     CASE "C*-*"
  59       >> /tell [Meister] [ROOM] ist der Spielraum der Stadt C[SUBST1]
  60     END
  61     CASE "C*"
  62       >> /tell [Meister] [ROOM] ist der Marktplatz der Stadt C[SUBST1]
  63     END
  64     CASE 0
  65       DO boerse
  66     CASE 16
  67     END
  68     CASE 28
  69     CASE [antwort] + 1
  70       DO manager
  71     CASE *
  72       >> /tell [Meister] [ROOM] ist ein Raum in ARMfeld
  73     END
  74     >> /tell [Meister] [SUBSTLEN] Teile - "[SUBST1]" "[SUBST2]" "[SUBST3]" - Spiel(e): [GAME] - Name: [NAME] - Owner: [OWNER]
  75     CASEALL *
  76       >> /tell [Meister] <EOT>
  77     END
  78   END
  79 END
  80
  81 ACTION boerse
  82   >> /tell [Meister] [ROOM] ist die Börse in ARMfeld
  83 END
  84
  85 ACTION manager
  86   >> /tell [Meister] [ROOM] ist der Spielemanager in ARMfeld
  87 END
  88
  89 ACTION varDump
  90   DEBUG ON
  91   VARDUMP
  92   DEBUG OFF
  93 END
  94
  95 ACTION eventDump
  96   DEBUG ON
  97   EVENTDUMP
  98   DEBUG OFF
  99 END
  100
  101 ACTION queueDump
  102   DEBUG ON
  103   QUEUEDUMP
  104   DEBUG OFF
  105 END
  106
  107 ACTION error
  108   >> Ein Fehler ist aufgetreten ([MESSAGE])
  109 END
  110
  111 PUPPETEND


:Quelle: BSW Puppet Forum >> Puppet Dokumentation vom Kugelschreiber - Antwort #26


.. |more| image:: /_static/more.png
          :align: middle
          :alt: more infoe.inc
