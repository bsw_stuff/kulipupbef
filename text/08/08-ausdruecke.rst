
.. _ausdruecke:

*********
Ausdrücke
*********

.. include:: 08-ausdruecke-01-variablen.inc
.. include:: 08-ausdruecke-02-boolsche.inc
.. include:: 08-ausdruecke-03-zahlen.inc
.. include:: 08-ausdruecke-04-strings.inc
.. include:: 08-ausdruecke-05-listen.inc
.. include:: 08-ausdruecke-06-sonstig.inc
.. include:: 08-ausdruecke-07-typpruefung.inc
.. include:: 08-ausdruecke-08-typumwand.inc
.. include:: 08-ausdruecke-09-auswertung.inc


.. note::

  Wichtig bei Ausdrücken ist, dass alle Symbole / Variablen / Konstanten etc. durch Leerzeichen voneinander abgetrennt werden.
