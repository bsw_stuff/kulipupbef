
Befehle zur Steuerung
=====================


.. index:: pair: SLEEP-Befehl; Befehl

SLEEP-Befehl
------------

::

  SLEEP <sek> [<millisek>]

- Verzögert die Ausführung um <sek> Sekunden und <millisek> Millisekunden.


.. tip::

  Diesen Befehl sollte man von Zeit zu Zeit ausführen, damit das Puppet den Server nicht zu sehr belastet. Während der Verzögerungszeit reagiert das Puppet auf nichts. - Auftretende Events werden danach abgearbeitet.


*Beispiel*::

  ACTION eineAction
    # Verzögerung von 2 Sekunden
    SLEEP 2
  END




.. index:: pair: MASTERRESET-Befehl; Befehl

MASTERRESET-Befehl
------------------

::

  MASTERRESET

- Führt einen Neustart des Puppets durch. Alle Variablen und alle `WHEN`-Einstellungen, sowie alle wartenden Events werden gelöscht.




.. index:: pair: HARAKIRI-Befehl; Befehl

HARAKIRI-Befehl
---------------

::

  HARAKIRI [<text>]

- Das Puppet beendet sich selbst.
- Der Parameter wird im Chat in der Form " -- [PuppetConsole] HARAKIRI: <text>" ausgegeben.
