
.. index:: pair: CHAT-Ereignis; Ereignis

CHAT-Ereignis
-------------

WHEN CHAT
^^^^^^^^^

::

  WHEN CHAT [erweiterte WHEN Bedingungen] DO <action>

- Das Puppet reagiert auf die Ausgabe von Text, indem die angegebene Action aufgerufen wird.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.


*Liste der belegten Variablen*:

* ``WHO`` = Sprecher des Textes
* ``CHAT`` = Eingegebener Text
* ``TYPE`` = Art des Chats (:ref:`Variable TYPE <VariableTYPE>`)
* ``ROOM`` = Aufenthaltsort des Sprechers (:ref:`Variable ROOM <VariableROOM>`)
* ``ROOMNAME`` = Angabe zum Aufenthaltsort des Sprechers (:ref:`Variable ROOMNAME <VariableROOMNAME>`)


*Beispiel*::

  ACTION eineAction
    # Registrierung diverser Actions
    #  - Reaktion auf Textausgaben von einer bestimmten Person
    WHEN CHAT FROM "Kugelschreiber" DO kuliAction
    #  - Reaktion auf Textausgaben von Personen, die in einer Liste stehen
    WHEN CHAT FROM "[Freunde]" DO freundeAction
    #  - Reaktion auf Textausgaben von Personen, zu denen sonst keinen Eintrag haben
    WHEN CHAT FROM "*" DO sonstAction
  END



IGNORE CHAT
^^^^^^^^^^^

::

  IGNORE CHAT [erweiterte WHEN Bedingungen]

- Das Puppet ignoriert ab sofort die Ausgabe von Text.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.


*Beispiel*::

  ACTION eineAction
    # De-Registrierung diverser Actions
    #  - Ignoriert Textausgaben einer bestimmten Person
    IGNORE CHAT FROM "Kugelschreiber"
    #  - Ignoriert Textausgaben der Personen, die in einer Liste stehen
    IGNORE CHAT FROM "[Freunde]"
    #  - Ignoriert alle Textausgaben
    IGNORE CHAT
  END



IGNOREALL CHAT
^^^^^^^^^^^^^^

::

  IGNOREALL CHAT [erweiterte WHEN Bedingungen]

- Das Puppet ignoriert ab sofort die Ausgabe von beliebigem Text.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.



.. index:: pair: KEYWORD-Ereignis; Ereignis

KEYWORD-Ereignis
----------------


WHEN KEYWORD
^^^^^^^^^^^^

::

  WHEN KEYWORD <keywordliste> [erweiterte WHEN Bedingungen] DO <action>

- Wenn das Puppet Text mitbekommt, welcher eines der Wörter aus <keywordliste> enthält, so wird die Action <action> ausgeführt.
- Das Wort muss dabei durch Leerzeichen vom Rest der Zeile abgegrenzt sein.
- Kommen mehrere (auch gleiche) KEYWORDs vor, so wird für jedes KEYWORD die zugehörige Action <action> (auch mehrmals) ausgeführt, und zwar von links  nach rechts.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.


*Liste der belegten Variablen*:

* ``WHO`` = Sprecher des Textes
* ``CHAT`` = Eingegebener Text
* ``TYPE`` = Art des Chats (:ref:`Variable TYPE <variableTYPE>`)
* ``ROOM`` = Aufenthaltsort des Sprechers (:ref:`Variable ROOM <variableROOM>`)
* ``ROOMNAME`` = Angabe zum Aufenthaltsort des Sprechers (:ref:`Variable ROOMNAME <variableROOMNAME>`)


*Beispiel*::

  ACTION start
    WHEN KEYWORD "!info" TYPE "TELL" DO info
    # Wenn zu zwei Quellen die selbe ACTION registriert werden soll,
    # kann man diese wie folgt zusammenfassen:
    WHEN KEYWORD "!hilfe" TYPE "GTELL TELL" DO hilfe
  END



IGNORE KEYWORD
^^^^^^^^^^^^^^

::

  IGNORE KEYWORD <liste> [erweiterte WHEN Bedingungen]

- Die Schlüsselwörter aus <keywordliste> werden ab sofort ignoriert bzw. nicht mehr gesondert behandelt, falls zu Schlüsselwort(en) noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.



IGNOREALL KEYWORD
^^^^^^^^^^^^^^^^^

::

  IGNOREALL KEYWORD [erweiterte WHEN Bedingungen]

- Das Puppet reagiert nicht mehr, bzw. nicht mehr gesondert auf beliebige  Schlüsselwörter.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.




.. index:: pair: MATCH-Ereignis; Ereignis

MATCH-Ereignis
--------------


.. _befehlWHENMATCH:

WHEN MATCH
^^^^^^^^^^

::

  WHEN MATCH <match> [erweiterte WHEN Bedingungen] DO <action>

- Wenn das Puppet Eingabezeilen mitbekommt, auf die <match> passt, so wird Action <action> ausgeführt.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.


*Liste der belegten Variablen*:

* ``WHO`` = Sprecher des Textes
* ``CHAT`` = Eingegebener Text
* ``SUBST1`` = Substitution für erstes * in ``MATCH``
* ``SUBST2`` = Substitution für zweites * in ``MATCH``
* ``SUBSTx`` = Substitution für x-tes * in ``MATCH``
* ``SUBSTLEN`` = Anzahl der Substitutionen 
* ``TYPE`` = Art des Chats (:ref:`Variable TYPE <variableTYPE>`)
* ``ROOM`` = Aufenthaltsort des Sprechers (:ref:`Variable ROOM <variableROOM>`)
* ``ROOMNAME`` = Angabe zum Aufenthaltsort des Sprechers (:ref:`Variable ROOMNAME <variableROOMNAME>`)



IGNORE  MATCH
^^^^^^^^^^^^^

::

  IGNORE MATCH <match> [erweiterte WHEN Bedingungen]

- Der <match> wird ab sofort nicht mehr überprüft bzw. nicht mehr gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.



IGNOREALL  MATCH
^^^^^^^^^^^^^^^^

::

  IGNOREALL MATCH [erweiterte WHEN Bedingungen]

- Der <match> wird ab sofort nicht mehr überprüft bzw. nicht mehr gesondert behandelt, falls noch eine "*"-Eintragung existiert.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.




.. index:: pair: CLICKED-Ereignis; Ereignis

CLICKED-Ereignis
----------------


WHEN CLICKED
^^^^^^^^^^^^

::

  WHEN CLICKED [erweiterte WHEN Bedingungen] DO <action>

- ...TO BE SPECIFIED...


*Liste der belegten Variablen*:

* ``WHO`` = Sprecher des Textes
* ``CHAT`` = Eingegebener Text
* ``ITEM`` = ???
* ``TYPE`` = Art des Chats (:ref:`Variable TYPE <variableTYPE>`)
* ``ROOM`` = Aufenthaltsort des Sprechers (:ref:`Variable ROOM <variableROOM>`)
* ``ROOMNAME`` = Angabe zum Aufenthaltsort des Sprechers (:ref:`Variable ROOMNAME <variableROOMNAME>`)



IGNORE CLICKED
^^^^^^^^^^^^^^

::

  IGNORE CLICKED [erweiterte WHEN Bedingungen]

- ...TO BE SPECIFIED...




.. index:: pair: NEWROOM-Ereignis; Ereignis

NEWROOM-Ereignis
----------------


WHEN NEWROOM
^^^^^^^^^^^^

::

  WHEN NEWROOM DO <action>

- Wenn das Puppet den Raum wechselt, wird die Action <action>  ausgeführt.


*Liste der belegten Variablen*:

* ``ROOM`` = ID des neuen Raumes



IGNORE NEWROOM
^^^^^^^^^^^^^^

::

  IGNORE NEWROOM

- Raumwechsel werden nicht mehr beachtet.




.. index:: pair: APPEAR-Ereignis; Ereignis

APPEAR-Ereignis
---------------


WHEN APPEAR
^^^^^^^^^^^

::

  WHEN APPEAR <liste> DO <action>

- Wenn jemand aus <liste> den Raum betritt wird die Action <action> ausgeführt.
- Zusätzlich geschieht dies durch die Eigenbewegung des Puppets.
- Die Wirkungsweise des Befehls kann durch :ref:`Erweiterte WHEN Bedingungen <erwWhenBed>` eingeschränkt werden.


.. note::

  Personen, deren Name mit "Geist" beginnt, werden ignoriert (=Leute, die connected haben, aber noch nicht eingeloggt sind).


*Liste der belegten Variablen*:

* ``WHO`` = Person, die den Raum betritt 



IGNORE APPEAR
^^^^^^^^^^^^^

::

  IGNORE APPEAR <liste>

- In Zukunft wird nichts gemacht, wenn jemand aus <liste> den Raum betritt.
- Genauer ausgedrückt: Es wird ein vorhergehendes ``WHEN APPEAR <liste> DO xxx`` gelöscht.

:Quelle: BSW Puppet Forum >> Ignore Appear - Antwort #1



IGNOREALL APPEAR
^^^^^^^^^^^^^^^^

::

  IGNOREALL APPEAR

- Alle APPEAR-Einträge werden gelöscht (default).




.. index:: pair: DISAPPEAR-Ereignis; Ereignis

DISAPPEAR-Ereignis
------------------


WHEN APPEAR
^^^^^^^^^^^

::

  WHEN DISAPPEAR <liste> DO <action>

- Wenn jemand aus <liste> den Raum verlässt wird die Action <action> ausgeführt.
- Zusätzlich geschieht dies durch die Eigenbewegung des Puppets. 


.. note::

  Personen, deren Name mit "Geist" beginnt, werden ignoriert (=Leute, die connected haben, aber noch nicht eingeloggt sind).


*Liste der belegten Variablen*:

* ``WHO`` = Person, die den Raum verlässt



IGNORE DISAPPEAR
^^^^^^^^^^^^^^^^

::

  IGNORE DISAPPEAR <liste>

- In Zukunft wird nichts gemacht, wenn jemand aus <liste> den Raum verlässt.
- Genauer ausgedrückt: Es wird ein vorhergehendes ``WHEN DISAPPEAR <liste> DO xxx``  gelöscht.



IGNOREALL DISAPPEAR
^^^^^^^^^^^^^^^^^^^

::

  IGNOREALL DISAPPEAR

- Alle DISAPPEAR-Einträge werden gelöscht (default).




.. index:: pair: TIME-Ereignis; Ereignis

TIME-Ereignis
-------------


WHEN TIME
^^^^^^^^^

::

  WHEN TIME <hour>[:<min>] DO <action>

- Jedes Mal zu der (Server-)Uhrzeit, die von <hour> und <min> bestimmt wird, wird die Action <action> ausgeführt. 


.. warning::

  Das Puppet kann sich nur eine Uhrzeit merken.



IGNORE TIME
^^^^^^^^^^^

::

  IGNORE TIME

- Puppet führt nichts mehr zu einer bestimmten Uhrzeit aus.




.. index:: pair: TIMER-Ereignis; Ereignis

TIMER-Ereignis
--------------


WHEN TIMER
^^^^^^^^^^

::

  WHEN TIMER <sek> DO <action>

- Alle <sek> Sekunden wird die Action <action> ausgeführt.


.. warning::

  Das Puppet kann sich nur einen Timer merken.



IGNORE TIMER
^^^^^^^^^^^^

::

  IGNORE TIMER

- Der Timer wird abgeschaltet.




.. index:: pair: ERROR-Ereignis; Ereignis

ERROR-Ereignis
--------------

.. _befehlWHENERROR:

WHEN ERROR
^^^^^^^^^^

::

  WHEN ERROR DO <action>

- Tritt ein Fehler bei der Ausführung eines Puppets auf (Teilen durch 0, etc.)  so wird die Action <action> ausgeführt (und zwar sofort und nicht erst nachdem die aktuelle Action beendet wurde). 


.. note::

  Tritt während der Ausführung der Action <action> ein Fehler auf, so wird diese nicht erneut ausgeführt. 


*Liste der belegten Variablen*:

* ``MESSAGE`` = Fehlermeldung (in der Regel eine Java-Fehlermeldung)



IGNORE ERROR
^^^^^^^^^^^^

::

  IGNORE ERROR

- Tritt ein Fehler auf, so wird nichts gemacht.




.. index:: pair: KICKED-Ereignis; Ereignis

KICKED-Ereignis
---------------


WHEN KICKED
^^^^^^^^^^^

::

  WHEN KICKED DO <action>

- Wenn jemand das Puppet kickt, hat das Puppet noch ca. 2 Sekunden Zeit ein  paar (Sleepy-)Befehle auszuführen, bevor es beendet wird. 
- Dafür wird sofort die Action <action> ausgeführt.
- Stehen noch andere Befehle an, so wird die Abarbeitung dieser zurückgestellt.

  
.. note::

  Sleepy-Befehle sind ``>>``, ``SLEEP``, ``MASTERRESET`` und ``HARAKIRI``. Ausserdem ist ca. jeder 100ste Befehl automatisch "sleepy".


*Liste der belegten Variablen*:

* ``WHO`` = Person, die das Puppet gekickt hat



IGNORE KICKED
^^^^^^^^^^^^^

::

  IGNORE KICKED

- Nichts passiert, wenn das Puppet gekickt wird.





.. index:: pair: PING-Ereignis; Ereignis

PING-Ereignis
-------------


WHEN PING
^^^^^^^^^

::

  WHEN PING [FROM <liste>] DO <action>

- Wenn jemand aus <liste> das Puppet anpingt, wird die Action  <action> ausgeführt.


*Liste der belegten Variablen*:

* ``WHO`` = Person (bzw. Puppet), die den Ping ausgelöst hat



IGNORE PING
^^^^^^^^^^^

::

  IGNORE PING [FROM <liste>]

- Wenn jemand aus <liste> das Puppet anpingt, wird nichts mehr gemacht.
- Wird <liste> ganz weggelassen, reagiert das Puppet auf überhaupt keine Pings mehr.




.. index:: pair: OWNED-Ereignis; Ereignis

OWNED-Ereignis
--------------


WHEN OWNED
^^^^^^^^^^

::

  WHEN OWNED [FROM <liste>] DO <action>

- Wenn jemand von <liste> das Puppet owned, wird die Action <action> ausgeführt.

  |more| Siehe :ref:`Puppet-Einstellung NEWOWNER <optionNewOwner>`.
  

*Liste der belegten Variablen*:

* ``WHO`` = Person, die das Puppet owned



IGNORE OWNED
^^^^^^^^^^^^

::

  IGNORE OWNED [FROM <liste>]

- Die Leute von <liste> werden nicht mehr beim ownen beachtet.
- Fehlt die Liste ganz, so wird überhaupt nicht mehr auf ownen geachtet.




.. index:: pair: UNOWNED-Ereignis; Ereignis

UNOWNED-Ereignis
----------------


WHEN UNOWNED
^^^^^^^^^^^^

::

  WHEN UNOWNED DO <action>

- Wenn das Puppet freigesetzt wird, wird die Action <action> ausgeführt.



IGNORE UNOWNED
^^^^^^^^^^^^^^

::

  IGNORE UNOWNED

- Beim Freisetzen des Puppets passiert nichts.
