
Befehle für Debugging
=====================


.. index:: pair: DEBUG-Befehl; Befehl

DEBUG-Befehl
------------

::

  DEBUG ON
  DEBUG OFF

* Schaltet den :ref:`Debugging-Modus <debuggingModus>` für einzelne Bereichs des Puppets an bzw. aus.
* Dies dient der Einschränkung der anfallenden Datenmengen.



.. index:: pair: VARDUMP-Befehl; Befehl

VARDUMP-Befehl
--------------

::

  VARDUMP

* Gibt alle Variablen und deren Belegung aus (``VD``).
* Der Befehl kann nur im aktivierten :ref:`Debugging-Modus <debuggingModus>` benutzt werden.


*Beispiel*::

  @vd: vardump
  # ...
  ACTION vardump
    DEBUG ON
    VARDUMP
    DEBUG OFF
  END



.. index:: pair: EVENTDUMP-Befehl; Befehl

.. _befehlEVENTDUMP:

EVENTDUMP-Befehl
----------------

::

  EVENTDUMP

* Gibt alle Events auf die das Puppet reagiert (WHEN-Befehle) aus (``ED..``). 
* Der Befehl kann nur im aktivierten :ref:`Debugging-Modus <debuggingModus>` benutzt werden.


*Beispiel*::

  @ed: eventdump
  # ...
  ACTION eventdump
    DEBUG ON
    EVENTDUMP
    DEBUG OFF
  END


*Format der Ausgaben*::

  EDCH <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
  EDKW <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
  EDMA <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
  EDCL <text> [FROM <who>] [TYPE <type>] [IN ROOM <room>] = (<action>)
  EDTR <action> [<interval>]
  EDKK <action>
  EDO <action> [FROM <who>]
  EDP <action> [FROM <who>]
  EDE <action>
  ...


* Kennnung =

  * ``ED..`` = EventDump
  * ``..CH`` = Event des Typs ``CHAT``
  * ``..KW`` = Event des Typs ``KEYWORD``
  * ``..MA`` = Event des Typs ``MATCH``
  * ``..CL`` = Event des Typs ``CLICKED``
  * ``..TR`` = Event des Typs ``TIMER``
  * ``..KK`` = Event des Typs ``KICKED``
  * ``..O`` = Event des Typs ``OWNED``
  * ``..P`` = Event des Typs ``PING``
  * ``..E`` = Event des Typs ``ERROR``

* <text> = Text/Keyword/Match für den ``CHAT`` des Events
* <who> = Einschränkung auf `WHO` (#, wenn nicht spezifiziert)
* <type> = Einschränkung auf `TYPE` (, wenn nicht spezifiziert)
* <room> = Einschränkung auf `ROOM`, also Raum/Kanal (#, wenn nicht spezifiziert)
* <intervall> = Länge des Intervalls
* <action> = Name der Action



.. index:: pair: QUEUEDUMP-Befehl; Befehl

QUEUEDUMP-Befehl
----------------

::

  QUEUEDUMP

* Gibt alle Befehle aus, die darauf warten ("Warteschlange"), vom Puppet ausgeführt zu werden (``QD``). 
* Der Befehl kann nur im aktivierten :ref:`Debugging-Modus <debuggingModus>` benutzt werden.


*Beispiel*::

  @qd: queuedump
  # ...
  ACTION queuedump
    DEBUG ON
    QUEUEDUMP
    DEBUG OFF
  END
